-- SELECT VilleArrivee FROM VOYAGES WHERE VilleDepart = 'Paris';

-- SELECT * FROM VOYAGES WHERE VilleArrivee = 'Amsterdam';

-- SELECT VilleDepart, TO_CHAR(Depart, 'DD-MM/HH24:MI') FROM VOYAGES 
-- WHERE VilleArrivee = 'Amsterdam';

-- SELECT Nom, VilleArrivee, Prix FROM CLIENTS
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN VOYAGES
-- ORDER BY Nom, Prix DESC; 

-- SELECT Nom, VilleDepart, Code FROM CLIENTS
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN VOYAGES
-- WHERE Ville = VilleDepart;

-- SELECT VilleDepart, VilleArrivee, TO_CHAR(Depart, 'DD-MM/HH24:MI')
-- FROM Voyages
-- WHERE Depart >= add_months(TRUNC(SYSDATE) + 1, 3)
-- ORDER BY Depart;

-- SELECT VilleDepart AS Ville FROM VOYAGES UNION (SELECT VilleArrivee AS Ville FROM VOYAGES);

-- SELECT * FROM CLIENTS WHERE NOT Ville = 'Paris';

-- SELECT * FROM CLIENTS 
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN Voyages
-- WHERE NOT Ville = 'Paris' AND VilleDepart = 'Paris';

-- SELECT * FROM CLIENTS
-- WHERE Id NOT IN (SELECT Id FROM RESERVATIONS);

-- SELECT * FROM Voyages
-- WHERE Code NOT IN (SELECT Code FROM RESERVATIONS);

-- SELECT DISTINCT Id, Nom, Prenom FROM CLIENTS
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN Voyages
-- WHERE VilleArrivee = 'Amsterdam' AND VilleDepart = 'Paris' 
-- MINUS SELECT DISTINCT Id, Nom, Prenom FROM CLIENTS
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN Voyages
-- WHERE VilleArrivee != 'Amsterdam' AND VilleDepart = 'Paris'; 

-- SELECT * FROM CLIENTS
-- WHERE Id 
-- IN (SELECT Id FROM CLIENTS
--  NATURAL JOIN RESERVATIONS
--  NATURAL JOIN Voyages
--  WHERE VilleArrivee = 'Amsterdam')
-- AND Id IN (SELECT Id FROM CLIENTS 
-- NATURAL JOIN Reservations
-- NATURAL JOIN Voyages
-- WHERE VilleArrivee = 'Rio de Janeiro');

-- SELECT DISTINCT Id, Nom, Prenom FROM CLIENTS
-- NATURAL JOIN RESERVATIONS
-- NATURAL JOIN Voyages 
-- WHERE VilleArrivee = 'Amsterdam' OR VilleArrivee = 'Rio de Janeiro';

-- SELECT c1.Id, c2.Id, c1.Ville FROM CLIENTS c1, CLIENTS c2
-- WHERE c1.Ville = c2.Ville AND C1.Id > c2.Id; 

-- SELECT v1.Code, v2.Code, v1.Prix FROM Voyages v1, Voyages v2
-- WHERE v1.Prix = v2.Prix AND v1.Code > v2.Code;

SELECT r1.ID
FROM RESERVATIONS r1, RESERVATIONS r2
WHERE r1.ID = r2.ID AND r1.Code < r2.Code;

SELECT ID FROM CLIENTS
MINUS (SELECT r1.ID
FROM RESERVATIONS r1, RESERVATIONS r2
WHERE r1.ID = r2.ID AND r1.Code < r2.Code
); 

-- SELECT DISTINCT r1.Id, r2.Id, r1.Code, r2.Code, TO_CHAR(r1.DateReserv, 'YY-MM-DD') FROM RESERVATIONS r1, RESERVATIONS r2
-- WHERE TO_CHAR(r1.DateReserv, 'YY-MM-DD') = TO_CHAR(r2.DateReserv, 'YY-MM-DD') AND r1.Id > r2.Id AND r1.Code > r2.Code;

-- SELECT DISTINCT r1.Id, r2.Id, r1.Code, TO_CHAR(r1.DateReserv, 'YY-MM-DD') FROM RESERVATIONS r1, RESERVATIONS r2
-- WHERE TO_CHAR(r1.DateReserv, 'YY-MM-DD') = TO_CHAR(r2.DateReserv, 'YY-MM-DD') AND r1.Id > r2.Id AND r1.Code = r2.Code;